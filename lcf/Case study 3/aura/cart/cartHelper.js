({
    contains : function(array, item, field) {
        for(var i=0; i < array.length;  i++) {
            if(array[i][field] == item[field])
                return true;
        }
        return false;
    }
})