({
    addItem : function(component, event, helper) {
        var products = component.get('v.products');
        var product = event.getParam('product');
        var broadcastMessageEvent = $A.get('e.c:broadcastMessage');
        if(helper.contains(products, product, 'Name')) {
            broadcastMessageEvent.setParams({
           message: 'Product already added :' + product.Name
            })
        } else {
            products.push(product);
            component.set('v.products', products);
            broadcastMessageEvent.setParams({
                message: 'Added ' + product.Name
            })
        }
        broadcastMessageEvent.fire();
    }
})