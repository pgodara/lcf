({
    handleBroadcastMessage : function(component, event, helper) {
        var message = event.getParam('message');
        component.set('v.message', message);

        setTimeout(function(){ 
            if(message == component.get('v.message')) 
                component.set('v.message', ''); 
        }, 3000);
    }
})