({
	search : function(component, event, helper) {
        component.getEvent('searchEvent').setParams({
            text: event.target.value
        }).fire();
	}
})