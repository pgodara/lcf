({
    doInit : function(component, event, helper) {      
        //Fetch the expense list from the Apex controller   
        helper.getProductList(component, '');
    },
    addToCart: function(component, event, helper) {
        //Get data via "data-data" attribute from button (button itself or icon's parentNode)
        var index = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        var addToCartEvent = $A.get('e.c:addToCart');
        addToCartEvent.setParams({
            product: component.get("v.displayProducts")[index]
        }).fire();
    },
    handlePaginateEvent: function(component, event, helper) {
        helper.setPage(component, event);
    },
    handleSearchEvent: function(component, event, helper) {
        helper.getProductList(component, event.getParam('text'));
    }
})