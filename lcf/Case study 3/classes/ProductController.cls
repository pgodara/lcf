public class ProductController {

   @AuraEnabled
   public static List<Product2> getProducts(String search) {
       search += '%';
       return [SELECT Id, Name, ProductCode, Family, Description FROM Product2 WHERE Name LIKE :search ORDER BY Name ASC];
   }
}