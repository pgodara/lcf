({
    send : function(component, event, helper) {
        var text = event.source.get("v.label");
        var applicationEvent = $A.get("e.c:applicationMessage");
        applicationEvent.setParams({
            text: text
        })
        applicationEvent.fire();
    }
})