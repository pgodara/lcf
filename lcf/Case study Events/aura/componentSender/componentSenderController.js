({
    send : function(component, event, helper) {
        var messageEvent = component.getEvent("messageEvent");
        console.log(event.source);
        var text = event.source.get("v.label");
        messageEvent.setParams({
            "text": text
        });
        messageEvent.fire();
    }
})