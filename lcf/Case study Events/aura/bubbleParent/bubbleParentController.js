({
    handleBubbling : function(component, event, helper) {
        console.log("Parent handling for " + event.getName());
        event.stopPropagation();
    }
})