public class ProductController {
  
  @AuraEnabled
  public static List<Product2> getProducts() {
    return [SELECT Id, Name, ProductCode FROM Product2 ORDER BY Name];
  }
}