({
    //Fetch the accounts from the Apex controller
    getProductList: function(component) {
        var action = component.get("c.getProducts");
        
        //Set up the callback
        action.setCallback(this, function(response) {
            var state = response.getState();
            $A.log(response);
            if(state === "SUCCESS"){
                component.set("v.products", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }   
})