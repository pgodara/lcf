({
    //Fetch the accounts from the Apex controller
    getProductList: function(component, search) {
        var action = component.get("c.getProducts");
        var itemsPerPage = component.get("v.itemsPerPage");
        
        action.setParams({
              "search": search
        });

        //Set up the callback
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result, i;
            var displayProducts = [];
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                var maxI = itemsPerPage;
                if(maxI > result.length)
            		maxI = result.length;
                component.set("v.products", result);
                component.set("v.totalItems", result.length);
                for( i = 0; i < maxI; i++ )
                    displayProducts.push(result[i]);
                component.set("v.displayProducts", displayProducts);
            }
        });
        $A.enqueueAction(action);
    },
    setPage: function(component, event) {
    	var page = event.getParam('page');
        var itemsPerPage = component.get("v.itemsPerPage");
        var i = ((page - 1) * itemsPerPage);
        var maxI = i + itemsPerPage;
        var result = component.get("v.products");
        if(maxI > result.length)
            maxI = result.length;
        var displayProducts = [];
        for( ; i < maxI; i++ )
            displayProducts.push(result[i]);
        component.set("v.displayProducts", displayProducts);
    }
})